#include "toolbox.h"
#include <userint.h>
#include "FullConfirmPopup.h"

#define Margin 2		// 2 pixels around elements
#define MAX(a,b) ((a)>=(b)?(a):(b))

static int Pnl=0;
static int CtrlYes=0, CtrlYesToAll=0, CtrlNo=0, CtrlNoToAll=0, CtrlCancel=0;
		
// Callback function of the buttons
int CVICALLBACK cb_FullConfirmPopup (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
				 if (control==CtrlYes)		QuitUserInterface(VAL_POPUP_YES); 
			else if (control==CtrlYesToAll)	QuitUserInterface(VAL_POPUP_YES_TO_ALL); 
			else if (control==CtrlNo)		QuitUserInterface(VAL_POPUP_NO); 
			else if (control==CtrlNoToAll)	QuitUserInterface(VAL_POPUP_NO_TO_ALL); 
			else if (control==CtrlCancel)	QuitUserInterface(VAL_POPUP_CANCEL);
			else QuitUserInterface(VAL_POPUP_CANCEL);
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
// FUNCTION: FullConfirmPopup
// PURPOSE: Displays a prompt message in a dialog box and waits for the user
// 			to select the 'Yes', 'Yes to All', 'No', 'No to All' or 'Cancel' button.
// IN: Title: The title to be displayed on the dialog box.
//     Message: The message to be displayed in the dialog box.
//              To display a multi-line message, embed newline characters (\n) in the message string.
// RETURN: Contains the user's response:
//				VAL_POPUP_YES 		- User selected 'Yes'
//				VAL_POPUP_YES_TO_ALL- User selected 'Yes To All'
//				VAL_POPUP_NO		- User selected 'No'
//				VAL_POPUP_NO_TO_ALL	- User selected 'No To All'
//				VAL_POPUP_CANCEL	- User selected 'Cancel'
//			Negative values indicate an error. See QuitUserInterface
///////////////////////////////////////////////////////////////////////////////
int FullConfirmPopup(const char* Title, const char* Message) {
	int CtrlText, TxtWidth, TxtHeight, ScrWidth, ScrHeight, Height, Width;
	int Left=Margin, CtrlHeight, CtrlWidth;
	int Res, Position;
	
	// Create the panel
	Pnl = NewPanel (0, Title, 0, 0, 400, 400);
	CtrlText = NewCtrl (Pnl, CTRL_TEXT_MSG, Message, Margin, Margin);
	
	GetCtrlAttribute(Pnl, CtrlText, ATTR_WIDTH, &TxtWidth);
	GetCtrlAttribute(Pnl, CtrlText, ATTR_HEIGHT, &TxtHeight);

	// Create the buttons
	CtrlYes = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__Yes", TxtHeight+2*Margin, Left);
	GetCtrlAttribute(Pnl, CtrlYes, ATTR_WIDTH, &CtrlWidth); Left+=CtrlWidth+Margin;
	
	CtrlYesToAll = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "Yes to __All", TxtHeight+2*Margin, Left);
	GetCtrlAttribute(Pnl, CtrlYesToAll, ATTR_WIDTH, &CtrlWidth); Left+=CtrlWidth+Margin;
	
	CtrlNo = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__No", TxtHeight+2*Margin, Left);
	GetCtrlAttribute(Pnl, CtrlNo, ATTR_WIDTH, &CtrlWidth); Left+=CtrlWidth+Margin;

	CtrlNoToAll = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "No to All", TxtHeight+2*Margin, Left);
	GetCtrlAttribute(Pnl, CtrlNoToAll, ATTR_WIDTH, &CtrlWidth); Left+=CtrlWidth+Margin;

	CtrlCancel = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__Cancel", TxtHeight+2*Margin, Left);
	GetCtrlAttribute(Pnl, CtrlCancel, ATTR_WIDTH, &CtrlWidth); Left+=CtrlWidth+Margin;

	GetCtrlAttribute(Pnl, CtrlYes, ATTR_HEIGHT, &CtrlHeight);

	// Dimension and position panel
	SetPanelSize(Pnl, Height=TxtHeight+3*Margin+CtrlHeight, Width=MAX(TxtWidth+2*Margin, Left));
	GetScreenSize (&ScrHeight, &ScrWidth);
	SetPanelPos (Pnl, ScrHeight/2-Height/2, ScrWidth/2-Width/2);
	
// This is optional for distributing the buttons horizontally
//	Position=Margin;
//	if (TxtWidth+2*Margin > Left) 
//		DistributeCtrls (Pnl, VAL_TB_HORIZONTAL_DISTRIBUTION,
//						   VAL_TB_FIXED_GAP_SPACING, &Position, (TxtWidth+2*Margin-Left)/4, 5,
//   							CtrlYes, CtrlYesToAll, CtrlNo, CtrlNoToAll, CtrlCancel);

	SetCtrlAttribute (Pnl, CtrlYes     , ATTR_CALLBACK_FUNCTION_POINTER, cb_FullConfirmPopup);
	SetCtrlAttribute (Pnl, CtrlYesToAll, ATTR_CALLBACK_FUNCTION_POINTER, cb_FullConfirmPopup);
	SetCtrlAttribute (Pnl, CtrlNo      , ATTR_CALLBACK_FUNCTION_POINTER, cb_FullConfirmPopup);
	SetCtrlAttribute (Pnl, CtrlNoToAll , ATTR_CALLBACK_FUNCTION_POINTER, cb_FullConfirmPopup);
	SetCtrlAttribute (Pnl, CtrlCancel  , ATTR_CALLBACK_FUNCTION_POINTER, cb_FullConfirmPopup);

	InstallPopup(Pnl);

	Res = RunUserInterface();
	
	RemovePopup (0);
	DiscardPanel(Pnl);
	return Res;
}
