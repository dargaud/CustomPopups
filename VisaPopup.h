#ifndef _VISA_POPUP
#define _VISA_POPUP

#include <visa.h>

extern int VisaPopup(ViSession Rsrc, ViString Expression, char* Descr,
					 const char* Title, const char* Message);

#endif
