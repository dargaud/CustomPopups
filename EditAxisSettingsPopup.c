///////////////////////////////////////////////////////////////////////////////
// MODULE	EditAxisSettingsPopup
// PURPOSE	Presents a popup that allows changing a few options of a graph axis
//			The complete list of changeable properties can be found in the axis editor of the UIR interface...
//			Currente changeable properties are limited to:
//			Precision, numerical format, show grid, log scale, mark origin, reverse axis...
//			Many more could be added
// AUTHOR	(c) 2012-2022 Guillaume Dargaud - Free use
//			http://www.gdargaud.net/Hack/LabWindows.html
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>

#include <userint.h>
#include <iso646.h>

#include "EditAxisSettingsPopup.h"

#define Margin 2		// 2 pixels around elements

// Temporary panel and controls created for the popup
static int Pnl=0;
static int CtrlOK=0,  CtrlCancel=0, 	// Buttons
		CtrlFormat=0, CtrlPrecision=0, CtrlGrid=0, CtrlLog=0,
		CtrlMarkOrig=0, CtrlReverse=0, CtrlName=0, CtrlScaling=0;	// Properties
static int IsStrip=0, IsDigital=0;

// Characteristics
static int Format, Precision, Grid, Log, MarkOrig, Reverse, Scaling,
	OriginalFormat,   OriginalPrecision, OriginalGrid, OriginalLog,
	OriginalMarkOrig, OriginalReverse,   OriginalScaling;
static char Name[31]="", OriginalName[31]="";

// Names of the attributes (which depend on the active X/Y axis)
static int OriginalPnl, OriginalCtrl,
	ActiveAxis, OrigActiveAxis,
	AttrActive=0, AttrFormat=0,   AttrPrecision=0, AttrGrid=0,
	AttrLog=0,    AttrMarkOrig=0, AttrReverse=0,   AttrName=0;

// Which ones are user settable
static int CopyAllow=0;

///////////////////////////////////////////////////////////////////////////////
///	HIFN	Return the absolute screen position of a (possible child) panel
///////////////////////////////////////////////////////////////////////////////
static void GetPanelAbsolutePos(int Panel, int* Top, int* Left) {
	int T, L;
	*Top=*Left=0;
	while (Panel!=0) {
		GetPanelAttribute(Panel, ATTR_TOP,  &T); *Top +=T;
		GetPanelAttribute(Panel, ATTR_LEFT, &L); *Left+=L;
		GetPanelAttribute(Panel, ATTR_PANEL_PARENT, &Panel);
	}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Callback function of the buttons, quit with or without committing the results
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_EditAxisSettingsPopup(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			if (control==CtrlCancel) QuitUserInterface(0);
			if (control==CtrlOK)     QuitUserInterface(1);
			break;
	}
	panel=eventData1=eventData2=0;	// Avoid warning
	callbackData=NULL;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Callback function to update the plot axis
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_EditAxisSettingsSamplePopup(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			SetCtrlAttribute (OriginalPnl, OriginalCtrl, AttrActive, ActiveAxis);

			#define DISP(F, p, f) if (control==Ctrl##f and CopyAllow & EASP_##F) \
				GetCtrlVal      (Pnl,         Ctrl##f,              p f), \
				SetCtrlAttribute(OriginalPnl, OriginalCtrl, Attr##f, f)

			DISP(FORMAT, 	&, Format);
			DISP(PRECISION,	&, Precision);
			DISP(GRID,		&, Grid);
			DISP(LOG,		&, Log);
			DISP(MARK,		&, MarkOrig);
			DISP(REVERSE,	&, Reverse);
			DISP(NAME,		 , Name);

			if (control==CtrlScaling and CopyAllow & EASP_SCALING)
				GetCtrlVal        (Pnl,         CtrlScaling,             &Scaling),
				SetAxisScalingMode(OriginalPnl, OriginalCtrl, ActiveAxis,
								   Scaling==VAL_MANUAL?VAL_LOCK:Scaling, 0, 0);

			SetCtrlAttribute(OriginalPnl, OriginalCtrl, AttrActive, OrigActiveAxis);
			break;
	}
	panel=eventData1=eventData2=0;	// Avoid warning
	callbackData=NULL;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Instead of placing the popup in the middle of the screen,
/// HIFN	place it near the proper axis for better readability
/// HIPAR	Pop / Panel handle of the future popup
///////////////////////////////////////////////////////////////////////////////
static void SetPopupPos(int OrigPnl, int OrigCtrl, int Axis, int Pop) {
	int /*ScrHeight, ScrWidth,*/ PoT, PoL, T, Thick, FH, L, W, H, PaT, PaL, PaW, PaH, PW, PH;
//	GetScreenSize (&ScrHeight, &ScrWidth);

//	GetPanelAttribute(OrigPnl, ATTR_TOP,   &PoT);
//	GetPanelAttribute(OrigPnl, ATTR_LEFT,  &PoL);
	GetPanelAbsolutePos(OrigPnl, &PoT, &PoL);

	GetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_TOP,    &T);
	GetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_LEFT,   &L);
	GetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_WIDTH,  &W);
	GetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_HEIGHT, &H);
	GetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_PLOT_AREA_TOP,    &PaT);
	GetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_PLOT_AREA_LEFT,   &PaL);
	GetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_PLOT_AREA_WIDTH,  &PaW);
	GetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_PLOT_AREA_HEIGHT, &PaH);

	GetPanelAttribute(Pop, ATTR_WIDTH,  &PW);
	GetPanelAttribute(Pop, ATTR_HEIGHT, &PH);
//	This is not ATTR_FRAME_ACTUAL_HEIGHT, ATTR_TITLE_POINT_SIZE, ATTR_FRAME_THICKNESS or ATTR_TITLEBAR_THICKNESS
	GetPanelAttribute (Pop, ATTR_TITLEBAR_ACTUAL_THICKNESS, &Thick);
	GetPanelAttribute (Pop, ATTR_FRAME_ACTUAL_HEIGHT,       &FH);

	switch (Axis) {
		case VAL_BOTTOM_XAXIS:SetPanelPos (Pop, PoT+T+PaT+ PaH-PH,    PoL+L+PaL+(PaW-PH)/2); break;
		case VAL_TOP_XAXIS:   SetPanelPos (Pop, PoT+T+PaT+Thick+FH,   PoL+L+PaL+(PaW-PH)/2); break;
		case VAL_LEFT_YAXIS:  SetPanelPos (Pop, PoT+T+PaT+(PaH-PH)/2, PoL+L+PaL); break;
		case VAL_RIGHT_YAXIS: SetPanelPos (Pop, PoT+T+PaT+(PaH-PH)/2, PoL+L+PaL+PaW-PW); break;
		default:SetPanelPos (Pop, PoT+T+(H-PH)/2, PoL+L+(W-PW)/2);	// Display in the middle of the screen
				break;
	}
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Displays a prompt message in a dialog box and waits for the user
/// HIFN	to change the precision and/or format type for a numeric control
/// HIFN	NOTE: this function needs to change the active axis and might thus
/// HIFN	NOTE: interfere with timer plots on the graph
/// HIPAR	OrigPnl/Panel where the control is located
/// HIPAR	OrigCtrl/Numeric control to change. Some numeric types may have limitations
/// HIPAR	Axis/One of VAL_LEFT_YAXIS, VAL_BOTTOM_XAXIS, VAL_RIGHT_YAXIS, VAL_TOP_XAXIS
/// HIPAR	Allow/Bitfield of which attributes to present for change
/// HIRET	1 if either has been changed, 0 if none, <0 if error (standard NI error code)
///////////////////////////////////////////////////////////////////////////////
int EditAxisSettingsPopup(int OrigPnl, int OrigCtrl, int Axis, int Allow) {
	int CtrlText, MsgHeight, /*ScrWidth, ScrHeight,*/ Height, Width, PrecisionMin=-1 /*Auto*/;
	int Style, Res, Err=0, CurrentTop=Margin;
	char Str[255], *Label="", GraphLabel[255], *StyleName;
	int L, W, T, H, LH;

	CopyAllow   =Allow;
	OriginalPnl =OrigPnl;
	OriginalCtrl=OrigCtrl;
	IsStrip=IsDigital=0;

	GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_CTRL_STYLE, &Style);
	switch (Style) {
		case CTRL_STRIP_CHART:
		case CTRL_STRIP_CHART_LS:	IsStrip=1;   StyleName="strip chart"; break;	// Some properties not available on X axis
		case CTRL_DIGITAL_GRAPH:
		case CTRL_DIGITAL_GRAPH_LS:	IsDigital=1; StyleName="digital graph"; break;	// X axis only
		case CTRL_GRAPH:
		case CTRL_GRAPH_LS:	                     StyleName="graph"; break;
		default:	return UIEValueIsInvalidOrOutOfRange;
	}

	if (IsStrip)   { Allow&= ~EASP_MARK;               PrecisionMin=0; }
	if (IsDigital) { Allow&=~(EASP_LOG|EASP_REVERSE);  PrecisionMin=0; }

	switch (Axis) {
		case VAL_BOTTOM_XAXIS:
		case VAL_TOP_XAXIS:		AttrActive=		ATTR_ACTIVE_XAXIS;
								AttrFormat=		ATTR_XFORMAT;
								AttrPrecision=	ATTR_XPRECISION;
								AttrGrid=		ATTR_XGRID_VISIBLE;
								AttrLog=		ATTR_XMAP_MODE;
								AttrMarkOrig=	ATTR_XMARK_ORIGIN;
								AttrReverse=	ATTR_XREVERSE;
								AttrName=		ATTR_XNAME;
								if (IsStrip) { Allow&=~(EASP_LOG|EASP_REVERSE); }
								break;
		case VAL_LEFT_YAXIS:
		case VAL_RIGHT_YAXIS:	AttrActive=		ATTR_ACTIVE_YAXIS;
								AttrFormat=		ATTR_YFORMAT;
								AttrPrecision=	ATTR_YPRECISION;
								AttrGrid=		ATTR_YGRID_VISIBLE;
								AttrLog=		ATTR_YMAP_MODE;
								AttrMarkOrig=	ATTR_YMARK_ORIGIN;
								AttrReverse=	ATTR_YREVERSE;
								AttrName=		ATTR_YNAME;
								if (IsDigital) { // The only one allowed to change is the name
									Allow&=~(EASP_FORMAT|EASP_PRECISION|EASP_GRID|EASP_MARK|EASP_REVERSE); }
								break;
		default:return UIEValueIsInvalidOrOutOfRange;
	}

	// Sanity checks
	if ((Allow&EASP_FORMAT)   ==0 and
		(Allow&EASP_PRECISION)==0 and
		(Allow&EASP_GRID)     ==0 and
		(Allow&EASP_LOG)      ==0 and
		(Allow&EASP_MARK)     ==0 and
		(Allow&EASP_REVERSE)  ==0 and
		(Allow&EASP_NAME)     ==0 and
		(Allow&EASP_SCALING)  ==0)
		return UIEInvalidAttribute;	// Nothing asked

		GetCtrlAttribute (OrigPnl, OrigCtrl, AttrActive, &OrigActiveAxis);	// To set it back later
	Err=SetCtrlAttribute (OrigPnl, OrigCtrl, AttrActive,  ActiveAxis=Axis);
	if (Err<0) return Err;	// Probably wrong type of control

	switch (Axis) {
		case VAL_BOTTOM_XAXIS:Label="Bottom X axis"; break;
		case VAL_TOP_XAXIS:   Label="Top X axis";    break;
		case VAL_LEFT_YAXIS:  Label="Left Y axis";   break;
		case VAL_RIGHT_YAXIS: Label="Right Y axis";  break;
		default:return UIEValueIsInvalidOrOutOfRange;
	}


	if (Allow&EASP_FORMAT) Err = GetCtrlAttribute (OrigPnl, OrigCtrl, AttrFormat, &Format);
	else Format=VAL_FLOATING_PT_FORMAT;
	OriginalFormat=Format;
	if (Err<0) return Err;	// Probably wrong type of control
	if (Format!=VAL_FLOATING_PT_FORMAT and Format!=VAL_SCIENTIFIC_FORMAT and Format!=VAL_ENGINEERING_FORMAT)
		return UIEControlNotTypeExpectedByFunction;			// Possibly axis set for text or time labels, which we ignore here

	// Check that the parameters we want to change are indeed available
	if (Allow&EASP_PRECISION) Err = GetCtrlAttribute (OrigPnl, OrigCtrl, AttrPrecision, &Precision);
	else Precision=0;
	OriginalPrecision=Precision;	// VAL_AUTO for Graphs but not for strip charts
	if (Err<0) return Err;	// Probably wrong type of control
	if (Precision<VAL_AUTO or 32<Precision) return -1;

	if (Allow&EASP_GRID) Err = GetCtrlAttribute (OrigPnl, OrigCtrl, AttrGrid, &Grid);
	else Grid=0;
	OriginalGrid=Grid;
	if (Err<0) return Err;	// Probably wrong type of control

	if (Allow&EASP_LOG) Err = GetCtrlAttribute (OrigPnl, OrigCtrl, AttrLog, &Log);
	else Log=0;
	OriginalLog=Log;
	if (Err<0) return Err;	// Probably wrong type of control

	if (Allow&EASP_MARK) Err = GetCtrlAttribute (OrigPnl, OrigCtrl, AttrMarkOrig, &MarkOrig);
	else MarkOrig=0;
	OriginalMarkOrig=MarkOrig;
	if (Err<0) return Err;	// Probably wrong type of control

	if (Allow&EASP_REVERSE) Err = GetCtrlAttribute (OrigPnl, OrigCtrl, AttrReverse, &Reverse);
	else Reverse=0;
	OriginalReverse=Reverse;
	if (Err<0) return Err;	// Probably wrong type of control

	if (Allow&EASP_NAME) Err = GetCtrlAttribute (OrigPnl, OrigCtrl, AttrName, Name);
	else Name[0]='\0';
	strcpy(OriginalName, Name);
	if (Err<0) return Err;	// Probably wrong type of control

	// VAL_NO_CHANGE -1, VAL_MANUAL 0, VAL_AUTOSCALE 1, VAL_LOCK 2
	// Only manual and autoscale returned here
	if (Allow&EASP_SCALING) Err = GetAxisScalingMode(OrigPnl, OrigCtrl, Axis, &Scaling, NULL, NULL /*&CurrentMin, &CurrentMax*/);
	else { Scaling=0; /*CurrentMin=CurrentMax=0;*/ }
	OriginalScaling=Scaling;
	if (Err<0) return Err;	// Probably wrong type of control


	// Create the panel
	Pnl = NewPanel (0, "Edit Axis Settings Popup", 0, 0, 200, Width=200);
	#define Raise(Ctrl) GetCtrlAttribute(Pnl, Ctrl, ATTR_HEIGHT, &Height); CurrentTop+=Height+Margin
	#define LabelOnRight(Ctrl)	GetCtrlAttribute(Pnl, Ctrl, ATTR_LEFT,         &L); \
								GetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH,        &W); \
								SetCtrlAttribute(Pnl, Ctrl, ATTR_LABEL_LEFT,    L+W+3); \
								GetCtrlAttribute(Pnl, Ctrl, ATTR_TOP,          &T); \
								GetCtrlAttribute(Pnl, Ctrl, ATTR_HEIGHT,       &H); \
								GetCtrlAttribute(Pnl, Ctrl, ATTR_LABEL_HEIGHT, &LH); \
								SetCtrlAttribute(Pnl, Ctrl, ATTR_LABEL_TOP,     T+(H-LH)/2);

	GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_LABEL_TEXT, GraphLabel);
	sprintf(Str, "Set attributes for %s of %s %s", Label, StyleName, GraphLabel);
	CtrlText      = NewCtrl (Pnl, CTRL_TEXT_MSG, Str, Margin, Margin);
	GetCtrlAttribute(Pnl, CtrlText, ATTR_HEIGHT, &MsgHeight);
	Raise(CtrlText);

	int PanelWidth;
	GetPanelAttribute(Pnl,           ATTR_WIDTH, &PanelWidth);
	GetCtrlAttribute (Pnl, CtrlText, ATTR_LEFT,  &L);
	GetCtrlAttribute (Pnl, CtrlText, ATTR_WIDTH, &W);
	if (PanelWidth<L+W) SetPanelAttribute(Pnl, ATTR_WIDTH, PanelWidth=L+W+3);

	// Create the inputs and the sample
	CtrlFormat    = NewCtrl (Pnl, CTRL_RING_LS,    "Format", CurrentTop, Margin);
	InsertListItem (Pnl, CtrlFormat, -1, "Floating point",VAL_FLOATING_PT_FORMAT);
	InsertListItem (Pnl, CtrlFormat, -1, "Scientific",	  VAL_SCIENTIFIC_FORMAT);
	InsertListItem (Pnl, CtrlFormat, -1, "Engineering",	  VAL_ENGINEERING_FORMAT);
	SetCtrlVal     (Pnl, CtrlFormat,    Format);
	Raise(CtrlFormat); LabelOnRight(CtrlFormat);

	CtrlPrecision = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Precision", CurrentTop, Margin);
	SetCtrlAttribute(Pnl, CtrlPrecision, ATTR_DATA_TYPE, VAL_INTEGER);
	SetCtrlAttribute(Pnl, CtrlPrecision, ATTR_MIN_VALUE, PrecisionMin);
	SetCtrlAttribute(Pnl, CtrlPrecision, ATTR_MAX_VALUE, 32);
	SetCtrlVal      (Pnl, CtrlPrecision, Precision);
	Raise(CtrlPrecision); LabelOnRight(CtrlPrecision);

	CtrlLog       = NewCtrl (Pnl, CTRL_RING_LS,    "Map mode", CurrentTop, Margin);
	InsertListItem (Pnl, CtrlLog, -1, "Linear", VAL_LINEAR);
	InsertListItem (Pnl, CtrlLog, -1, "Log",    VAL_LOG);
	SetCtrlVal     (Pnl, CtrlLog, Log);
	Raise(CtrlLog); LabelOnRight(CtrlLog);

	CtrlGrid = NewCtrl (Pnl, CTRL_CHECK_BOX, "Show grid", CurrentTop, Margin);
	SetCtrlVal         (Pnl, CtrlGrid, Grid);
	Raise(CtrlGrid); LabelOnRight(CtrlGrid);

	CtrlMarkOrig = NewCtrl (Pnl, CTRL_CHECK_BOX, "Mark origin", CurrentTop, Margin);
	SetCtrlVal             (Pnl, CtrlMarkOrig, MarkOrig);
	Raise(CtrlMarkOrig); LabelOnRight(CtrlMarkOrig);

	CtrlReverse = NewCtrl (Pnl, CTRL_CHECK_BOX, "Reverse axis", CurrentTop, Margin);
	SetCtrlVal            (Pnl, CtrlReverse, Reverse);
	Raise(CtrlReverse); LabelOnRight(CtrlReverse);

	CtrlScaling       = NewCtrl (Pnl, CTRL_RING_LS,    "Scaling mode", CurrentTop, Margin);
	InsertListItem (Pnl, CtrlScaling, -1, "Lock", VAL_LOCK);
	InsertListItem (Pnl, CtrlScaling, -1, "Auto", VAL_AUTOSCALE);
	SetCtrlVal     (Pnl, CtrlScaling, Scaling);
	Raise(CtrlScaling); LabelOnRight(CtrlScaling);

	CtrlName = NewCtrl (Pnl, CTRL_STRING_LS, "Axis name", CurrentTop, Margin);
	SetCtrlAttribute   (Pnl, CtrlName, ATTR_MAX_ENTRY_LENGTH, 30);
	SetCtrlVal         (Pnl, CtrlName, Name);
	Raise(CtrlName); LabelOnRight(CtrlName);

	SetCtrlAttribute(Pnl, CtrlFormat,    ATTR_DIMMED, !(Allow&EASP_FORMAT));
	SetCtrlAttribute(Pnl, CtrlPrecision, ATTR_DIMMED, !(Allow&EASP_PRECISION));
	SetCtrlAttribute(Pnl, CtrlLog,       ATTR_DIMMED, !(Allow&EASP_LOG));
	SetCtrlAttribute(Pnl, CtrlGrid,      ATTR_DIMMED, !(Allow&EASP_GRID));
	SetCtrlAttribute(Pnl, CtrlMarkOrig,  ATTR_DIMMED, !(Allow&EASP_MARK));
	SetCtrlAttribute(Pnl, CtrlReverse,   ATTR_DIMMED, !(Allow&EASP_REVERSE));
	SetCtrlAttribute(Pnl, CtrlName,      ATTR_DIMMED, !(Allow&EASP_NAME));
	SetCtrlAttribute(Pnl, CtrlScaling,   ATTR_DIMMED, !(Allow&EASP_SCALING));

	cb_EditAxisSettingsSamplePopup(0, 0, EVENT_COMMIT, 0, 0, 0);	// Set the initial state

	// Create the buttons
	CtrlOK     = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__OK",     CurrentTop,   Width/4);
	CtrlCancel = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__Cancel", CurrentTop, 3*Width/4);
	SetCtrlAttribute (Pnl, CtrlCancel, ATTR_SHORTCUT_KEY, VAL_ESC_VKEY);
	SetPanelAttribute(Pnl, ATTR_CLOSE_CTRL, CtrlCancel);
	Raise(CtrlOK);
	GetCtrlAttribute(Pnl, CtrlOK,     ATTR_WIDTH, &W);
	SetCtrlAttribute(Pnl, CtrlOK,     ATTR_LEFT, Width/4-W/2);
	GetCtrlAttribute(Pnl, CtrlCancel, ATTR_WIDTH, &W);
	SetCtrlAttribute(Pnl, CtrlCancel, ATTR_LEFT, 3*Width/4-W/2);

	// Dimension and position panel
	SetPanelAttribute(Pnl, ATTR_HEIGHT, CurrentTop);
	SetPopupPos(OrigPnl, OrigCtrl, Axis, Pnl);

	SetCtrlAttribute(Pnl, CtrlOK,		ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsPopup);
	SetCtrlAttribute(Pnl, CtrlCancel,	ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsPopup);

	SetCtrlAttribute(Pnl, CtrlPrecision,ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsSamplePopup);
	SetCtrlAttribute(Pnl, CtrlFormat,	ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsSamplePopup);
	SetCtrlAttribute(Pnl, CtrlGrid,		ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsSamplePopup);
	SetCtrlAttribute(Pnl, CtrlLog,		ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsSamplePopup);
	SetCtrlAttribute(Pnl, CtrlMarkOrig,	ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsSamplePopup);
	SetCtrlAttribute(Pnl, CtrlReverse,	ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsSamplePopup);
	SetCtrlAttribute(Pnl, CtrlName,		ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsSamplePopup);
	SetCtrlAttribute(Pnl, CtrlScaling,	ATTR_CALLBACK_FUNCTION_POINTER, cb_EditAxisSettingsSamplePopup);

	SetCtrlAttribute (OrigPnl, OrigCtrl, AttrActive, OrigActiveAxis);	// Reinstate in case there's a timer plot
	InstallPopup(Pnl);

	Res = RunUserInterface();	// Now we wait until cb_PrecisionPopup calls QuitUserInterface

	RemovePopup(0);
	DiscardPanel(Pnl);

	SetCtrlAttribute (OrigPnl, OrigCtrl, AttrActive, ActiveAxis);
	#define SET(F, f) if (Allow&EASP_##F and f!=Original##f) \
						SetCtrlAttribute (OrigPnl, OrigCtrl, Attr##f, Res ? f : Original##f)
	SET(FORMAT,    Format);
	SET(PRECISION, Precision);
	SET(GRID,      Grid);
	SET(LOG,       Log);
	SET(MARK,      MarkOrig);
	SET(REVERSE,   Reverse);
	SET(NAME,      Name);
	if (Allow&EASP_SCALING and Scaling!=OriginalScaling)
		SetAxisScalingMode(OrigPnl, OrigCtrl, ActiveAxis,
						   Scaling==VAL_MANUAL?VAL_LOCK:Scaling, 0, 0);

	SetCtrlAttribute (OrigPnl, OrigCtrl, AttrActive, OrigActiveAxis);

	return Res;
}


///////////////////////////////////////////////////////////////////////////////
///	HIFN	Optional axis properties popup depending where the user clicked
/// HIFN	(probably right-click but up to you) on the graph
/// HIFN	If close to an axis, with show the popup, otherwise ignores the call
/// HIFN	Example of use:
/// HIFN		case EVENT_RIGHT_CLICK:
/// HIFN			ChangeAxisProperties(panel, control, eventData1, eventData2);
/// HIFN			break;
///////////////////////////////////////////////////////////////////////////////
int ChangeAxisProperties(int panel, int control, int eventData1, int eventData2) {
	int H, W, L, T, X=eventData2, Y=eventData1, Axis=-1, PAH, PAW, PAL, PAT;
	GetCtrlAttribute(panel, control, ATTR_HEIGHT, &H); double NY=H;
	GetCtrlAttribute(panel, control, ATTR_WIDTH,  &W); double NX=W;
	GetCtrlAttribute(panel, control, ATTR_LEFT, &L); X-=L;
	GetCtrlAttribute(panel, control, ATTR_TOP,  &T); Y-=T;

	GetCtrlAttribute(panel, control, ATTR_PLOT_AREA_HEIGHT, &PAH);
	GetCtrlAttribute(panel, control, ATTR_PLOT_AREA_WIDTH,  &PAW);
	GetCtrlAttribute(panel, control, ATTR_PLOT_AREA_LEFT,   &PAL);
	GetCtrlAttribute(panel, control, ATTR_PLOT_AREA_TOP,    &PAT);

	// In order of probability
	if (X/NX<Y/NY and X/NX<1-Y/NY) {	                                           // Left
		if (X<=PAL    ) SetCtrlAttribute (panel, control, ATTR_ACTIVE_YAXIS, Axis=VAL_LEFT_YAXIS);
	} else if (X/NX<Y/NY and X/NX>1-Y/NY) {	                                       // Bottom
		if (Y>=PAT+PAH) SetCtrlAttribute (panel, control, ATTR_ACTIVE_XAXIS, Axis=VAL_BOTTOM_XAXIS);
	} else if (X/NX>Y/NY and X/NX>1-Y/NY) {	                                       // Right
		if (X>=PAL+PAW) SetCtrlAttribute (panel, control, ATTR_ACTIVE_YAXIS, Axis=VAL_RIGHT_YAXIS);
	} else if (X/NX>Y/NY and X/NX<1-Y/NY) {	                                       // Top
		if (Y<=PAT    ) SetCtrlAttribute (panel, control, ATTR_ACTIVE_XAXIS, Axis=VAL_TOP_XAXIS);
	}
	return Axis!=-1 ? EditAxisSettingsPopup(panel, control, Axis, EASP_ALL) : 0;
}
