///////////////////////////////////////////////////////////////////////////////
// MODULE	SlideRampPopup
// PURPOSE	Presents a popup that allows changing a few options of a slide-type numerical control (listed in AuthorizedStyles)
// LIMITS	Currently only works on VAL_DOUBLE or VAL_FLOAT types of numerics
// LIMITS	Currently only works on VAL_FLOATING_PT_FORMAT, VAL_SCIENTIFIC_FORMAT or VAL_ENGINEERING_FORMAT format of numerics
// AUTHOR	Guillaume Dargaud 
// (c)		GPL
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <userint.h>
#include <utility.h>
#include <iso646.h>

#include "SlideRampPopup.h"

#define Margin 2		// 2 pixels around elements
#define Step 50			// Number of pixels between each line of controls
#define MAX(a,b) ((a)>=(b)?(a):(b))

static int AuthorizedStyles[]={		// Should work on all of those, but some attributes won't work
		CTRL_NUMERIC_THERMOMETER,
		CTRL_NUMERIC_TANK,
		CTRL_NUMERIC_GAUGE,
		CTRL_NUMERIC_METER,
		CTRL_NUMERIC_KNOB,
		CTRL_NUMERIC_DIAL,
		CTRL_NUMERIC_VSLIDE,
		CTRL_NUMERIC_HSLIDE,
		CTRL_NUMERIC_FLAT_VSLIDE,
		CTRL_NUMERIC_FLAT_HSLIDE,
		CTRL_NUMERIC_LEVEL_VSLIDE,
		CTRL_NUMERIC_LEVEL_HSLIDE,
		CTRL_NUMERIC_POINTER_VSLIDE,
		CTRL_NUMERIC_POINTER_HSLIDE,
		CTRL_NUMERIC_THERMOMETER_LS,
		CTRL_NUMERIC_TANK_LS,
		CTRL_NUMERIC_GAUGE_LS,
		CTRL_NUMERIC_METER_LS,
		CTRL_NUMERIC_KNOB_LS,
		CTRL_NUMERIC_DIAL_LS,
		CTRL_NUMERIC_LEVEL_VSLIDE_LS,
		CTRL_NUMERIC_LEVEL_HSLIDE_LS,
		CTRL_NUMERIC_POINTER_VSLIDE_LS,
		CTRL_NUMERIC_POINTER_HSLIDE_LS
};

#if 0	// For testing
static char* StrStyles[]={
		"THERMOMETER",
		"TANK",
		"GAUGE",
		"METER",
		"KNOB",
		"DIAL",
		"VSLIDE",
		"HSLIDE",
		"FLAT_VSLIDE",
		"FLAT_HSLIDE",
		"LEVEL_VSLIDE",
		"LEVEL_HSLIDE",
		"POINTER_VSLIDE",
		"POINTER_HSLIDE",
		"THERMOMETER_LS",
		"TANK_LS",
		"GAUGE_LS",
		"METER_LS",
		"KNOB_LS",
		"DIAL_LS",
		"LEVEL_VSLIDE_LS",
		"LEVEL_HSLIDE_LS",
		"POINTER_VSLIDE_LS",
		"POINTER_HSLIDE_LS"
};
#endif

// Temporary panel and controls created for the popup
static int Pnl=0, OrigPnl_, OrigCtrl_;

// Which ones are user settable (see SRP_ bitfield)
static int CopyAllow=0;

static int CtrlOK=0,  CtrlCancel=0,
		// Various slider attribute, not all valid depending on style
		CtrlFillColor=0,        CtrlFillHousingColor=0,
		CtrlSliderColor=0,      CtrlSliderWidth=0,
		CtrlMarkerStartAngle=0, CtrlMarkerEndAngle=0,
		CtrlMinValue=0,			CtrlMaxValue=0,
		CtrlFormat=0,			CtrlPrecision=0,
		// Color ramp attributes
		CtrlThickness=0, 		CtrlInterpol=0, 	CtrlNb=0,
		CtrlColor[255]={0},  	CtrlLimit[255]={0}, CtrlColorInf=0;

// Attributes
static int FillColor,     	FillHousingColor,
		SliderColor,      	SliderWidth,
		MarkerStartAngle, 	MarkerEndAngle,
		NbColors, Thickness,Interpol, ColorInf, DataType,
		Format,				Precision;
static double MinValue,		MaxValue;

static ColorMapEntry ColorMapArray[255];

// Originals for cancel
static int OrigFillColor, 		OrigFillHousingColor,
		OrigSliderColor,  		OrigSliderWidth,
		OrigMarkerStartAngle, 	OrigMarkerEndAngle,
		OrigNbColors, OrigThickness, OrigInterpol, OrigColorInf,
		OrigFormat,				OrigPrecision;
static double OrigMinValue, OrigMaxValue;

static ColorMapEntry OrigColorMapArray[255];

///////////////////////////////////////////////////////////////////////////////
// Callback function of the buttons, quit with or without committing the results
int CVICALLBACK cb_SlideRampPopup (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			if (control==CtrlCancel) QuitUserInterface(0);
			if (control==CtrlOK)     QuitUserInterface(1); 
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Return the bottom position of a control
/// HIRET	Sum of Top+Height
///////////////////////////////////////////////////////////////////////////////
static int GetBottom(int Ctrl) {
	int Top, Height;
	if (Ctrl==0) return 0;
	GetCtrlAttribute(Pnl, Ctrl, ATTR_TOP,    &Top);
	GetCtrlAttribute(Pnl, Ctrl, ATTR_HEIGHT, &Height);
	return Top+Height;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_SlideRampChange(int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int i, Top;
	switch (event) {
		case EVENT_COMMIT:
			#define CheckCtrl(C, c)\
				if (control==Ctrl##c) {\
					GetCtrlVal(panel, control, &c);\
					SetCtrlAttribute(OrigPnl_, OrigCtrl_, ATTR_##C, c);\
					break;\
				}
			
			CheckCtrl(FILL_COLOR,         FillColor);
			CheckCtrl(FILL_HOUSING_COLOR, FillHousingColor);
			CheckCtrl(SLIDER_COLOR,       SliderColor);
			CheckCtrl(SLIDER_WIDTH,       SliderWidth);
			CheckCtrl(MARKER_START_ANGLE, MarkerStartAngle);
			CheckCtrl(MARKER_END_ANGLE,   MarkerEndAngle);
			CheckCtrl(MIN_VALUE,		  MinValue);	// TODO: handle float case
			CheckCtrl(MAX_VALUE,		  MaxValue);
			
			if (control==CtrlFormat) {
				GetCtrlVal(panel, control, &Format);
				SetCtrlAttribute(OrigPnl_, OrigCtrl_, ATTR_FORMAT, Format);
				if (CtrlMinValue)     SetCtrlAttribute(panel, CtrlMinValue, ATTR_FORMAT, Format);
				if (CtrlMaxValue)     SetCtrlAttribute(panel, CtrlMaxValue, ATTR_FORMAT, Format);
				for (i=0; i<255; i++) 
					if (CtrlLimit[i]) SetCtrlAttribute(panel, CtrlLimit[i], ATTR_FORMAT, Format);
				break;
			}
			if (control==CtrlPrecision) {
				GetCtrlVal(panel, control, &Precision);
				SetCtrlAttribute(OrigPnl_, OrigCtrl_, ATTR_PRECISION, Precision);
				if (CtrlMinValue)     SetCtrlAttribute(panel, CtrlMinValue, ATTR_PRECISION, Precision);
				if (CtrlMaxValue)     SetCtrlAttribute(panel, CtrlMaxValue, ATTR_PRECISION, Precision);
				for (i=0; i<255; i++) 
					if (CtrlLimit[i]) SetCtrlAttribute(panel, CtrlLimit[i], ATTR_PRECISION, Precision);
				break;
			}

			if (control==CtrlNb) {
				GetCtrlVal(panel, control, &NbColors);
				for (i=0; i<255; i++) 
					SetCtrlAttribute(panel, CtrlColor[i], ATTR_VISIBLE, i<NbColors),
					SetCtrlAttribute(panel, CtrlLimit[i], ATTR_VISIBLE, i<NbColors);
				GetCtrlAttribute(panel, CtrlColor[NbColors], ATTR_TOP, &Top);
				SetCtrlAttribute(panel, CtrlColorInf, ATTR_TOP, Top);
				SetPanelAttribute(panel, ATTR_HEIGHT, GetBottom(NbColors==0 ? CtrlNb : CtrlColorInf)+2);
				SetNumericColorRamp (OrigPnl_, OrigCtrl_, ColorMapArray, ColorInf, NbColors, NbColors>0 ? Interpol : 0);

				SetCtrlAttribute(panel, CtrlThickness,ATTR_VISIBLE, NbColors>0);
				SetCtrlAttribute(panel, CtrlInterpol, ATTR_VISIBLE, NbColors>0);
				SetCtrlAttribute(panel, CtrlColorInf, ATTR_VISIBLE, NbColors>0);
				break;
			}
			
			CheckCtrl(COLOR_RAMP_WIDTH,       Thickness);	// Not updated immediately on the UIR. NI bug ?
			CheckCtrl(COLOR_RAMP_INTERPOLATE, Interpol);

			if (control==CtrlColorInf) {
				GetCtrlVal(panel, control, &ColorInf);
				SetNumericColorRamp (OrigPnl_, OrigCtrl_, ColorMapArray, ColorInf, NbColors, Interpol);
				break;
			}
			
			for (i=0; i<NbColors; i++) {
				if (control==CtrlColor[i]) {
					GetCtrlVal(panel, control, &ColorMapArray[i].color);
					SetNumericColorRamp (OrigPnl_, OrigCtrl_, ColorMapArray, ColorInf, NbColors, Interpol);
					return 0;
				}
				if (control==CtrlLimit[i]) {
					if (DataType==VAL_DOUBLE) GetCtrlVal(panel, control, &ColorMapArray[i].dataValue.valDouble);
					if (DataType==VAL_FLOAT)  GetCtrlVal(panel, control, &ColorMapArray[i].dataValue.valFloat);
					SetNumericColorRamp (OrigPnl_, OrigCtrl_, ColorMapArray, ColorInf, NbColors, Interpol);
					return 0;
				}
			}
			break;
	}
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
// HIFN	 	Displays a prompt message in a dialog box and waits for the user
//			to change the characteristics of a numeric meter
// HIPAR	OrigPnl/Panel where the control is located
// HIPAR	OrigCtrl/Numeric meter control to change.
// HIRET	1 if either has been changed, 0 if none, <0 if error (standard NI error code)
///////////////////////////////////////////////////////////////////////////////
int SlideRampPopup(int OrigPnl, int OrigCtrl, int Allow) {
	int Ctrl, ScrWidth, ScrHeight, Height, Width;
	unsigned int i;
	int Res, Err, Style, /*Format, Precision,*/ Top=2, W=35, WW=75, SBLE;
	char Str[80];
	
	OrigPnl_ =OrigPnl;
	OrigCtrl_=OrigCtrl;
	CopyAllow=Allow;
	
	CtrlOK=CtrlCancel=
	CtrlFillColor=CtrlFillHousingColor=CtrlSliderColor=CtrlSliderWidth=
	CtrlMarkerStartAngle=CtrlMarkerEndAngle=
	CtrlMinValue=CtrlMaxValue=
	CtrlFormat=CtrlPrecision=
	CtrlThickness=CtrlInterpol=CtrlNb=CtrlColorInf=0;

	// Sanity checks
	Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_CTRL_STYLE,    &Style);
	for (i=0; i<sizeof(AuthorizedStyles)/4; i++)
		if (Style==AuthorizedStyles[i]) 
			goto Continue;
	return UIEInvalidControlType;

Continue:

	Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_DATA_TYPE,    &DataType);
	if (Err<0) return Err;	// Probably wrong type of control
	if (DataType!=VAL_DOUBLE and DataType!=VAL_FLOAT) return UIEInvalidControlType;
	
	Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_FORMAT,       &Format);
	if (Err<0) return Err;	// Probably wrong type of control
	if (Format!=VAL_FLOATING_PT_FORMAT and Format!=VAL_SCIENTIFIC_FORMAT and Format!=VAL_ENGINEERING_FORMAT)
		return UIEControlNotTypeExpectedByFunction;
	
	// Check that the parameters we want to change are indeed available
	Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_PRECISION,    &Precision);
	if (Err<0) return Err;	// Probably wrong type of control
	if (Precision<0 or 32<Precision) return UIEInvalidControlType;

	#define GetDef(C, c)\
		if (CopyAllow&SRP_##C) {\
			Err = GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_##C, &c);\
			if (Err<0) CopyAllow&=~SRP_##C; else Orig##c=c;\
		}

	SBLE = SetBreakOnLibraryErrors(0);

	GetDef(FILL_COLOR, 		   FillColor);
	GetDef(FILL_HOUSING_COLOR, FillHousingColor);
	GetDef(SLIDER_COLOR,	   SliderColor);
	GetDef(SLIDER_WIDTH,	   SliderWidth);
	GetDef(MIN_VALUE,	   	   MinValue);
	GetDef(MAX_VALUE,	   	   MaxValue);
	GetDef(FORMAT,	   	       Format);
	GetDef(PRECISION,	   	   Precision);
	
	if (CopyAllow&SRP_MARKER_ANGLES) {
		Err = GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_MARKER_START_ANGLE, &MarkerStartAngle);
		if (Err<0) CopyAllow&=~SRP_MARKER_ANGLES; else OrigMarkerStartAngle=MarkerStartAngle;
		Err = GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_MARKER_END_ANGLE, &MarkerEndAngle);
		OrigMarkerEndAngle=MarkerEndAngle;
	}
	
	if (CopyAllow&SRP_RAMP) {
		Err = GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_COLOR_RAMP_INTERPOLATE, &Interpol);
		if (Err<0) { CopyAllow&=~SRP_RAMP; goto NoRamp; }	// Probably wrong type of control
		OrigInterpol=Interpol;

		Err = GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_COLOR_RAMP_WIDTH, &Thickness);
		if (Err<0) { CopyAllow&=~SRP_RAMP; goto NoRamp; }
		OrigThickness=Thickness;

		Err = GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_NUM_COLOR_RAMP_VALUES, &NbColors);
		if (Err<0) { CopyAllow&=~SRP_RAMP; goto NoRamp; }
		OrigNbColors=NbColors;

		Err = GetNumericColorRamp (OrigPnl, OrigCtrl, ColorMapArray, &ColorInf, NULL /*&Interpol*/);
		if (Err<0) { CopyAllow&=~SRP_RAMP; goto NoRamp; }
		Err = GetNumericColorRamp (OrigPnl, OrigCtrl, OrigColorMapArray, &OrigColorInf, NULL /*&OrigInterpol*/);
	}
	SetBreakOnLibraryErrors (SBLE);
	
NoRamp:
	if (CopyAllow==0) return UIEInvalidControlType;	// Nothing to do !
	
	// Create the panel
	Pnl = NewPanel (0, "Slide ramp Customization", 0, 0, Height=186, Width=183);
	Ctrl=0;
	#define PlaceCtrl(Val)\
		SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH, W);\
		SetCtrlAttribute(Pnl, Ctrl, ATTR_LABEL_LEFT, W+3);\
		SetCtrlAttribute(Pnl, Ctrl, ATTR_LABEL_TOP, Top+4);\
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CTRL_VAL, Val);\
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CALLBACK_FUNCTION_POINTER, cb_SlideRampChange);

	
	// Create the input controls
	if (CopyAllow&SRP_FILL_COLOR) {
		CtrlFillColor = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Fill Color", Top=GetBottom(Ctrl)+2, 2);
		PlaceCtrl(FillColor);
	}
	if (CopyAllow&SRP_FILL_HOUSING_COLOR) {
		CtrlFillHousingColor = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Fill Housing Color", Top=GetBottom(Ctrl)+2, 2);
		PlaceCtrl(FillHousingColor);
	}
	
	if (CopyAllow&SRP_SLIDER_COLOR) {
		CtrlSliderColor = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Slider Color", Top=GetBottom(Ctrl)+2, 2);
		PlaceCtrl(SliderColor);
	}
	if (CopyAllow&SRP_SLIDER_WIDTH) {
		CtrlSliderWidth = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Slider Width", Top=GetBottom(Ctrl)+2, 2);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE,   VAL_INTEGER);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CHECK_RANGE, VAL_COERCE);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MIN_VALUE, 0);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MAX_VALUE, 32767); // Strangely high
		PlaceCtrl(SliderWidth);
	}
	if (CopyAllow&SRP_MIN_VALUE) {
		CtrlMinValue = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Min value", Top=GetBottom(Ctrl)+2, 2);
		PlaceCtrl(MinValue); 
		SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH, WW);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE, DataType);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_FORMAT,    Format);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_PRECISION, Precision);
	}
	if (CopyAllow&SRP_MAX_VALUE) {
		CtrlMaxValue = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Max value", Top=GetBottom(Ctrl)+2, 2);
		PlaceCtrl(MaxValue); 
		SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH, WW);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE, DataType);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_FORMAT,    Format);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_PRECISION, Precision);
	}
	if (CopyAllow&SRP_FORMAT) {
		CtrlFormat = Ctrl = NewCtrl (Pnl, CTRL_RING_LS, "Format", Top=GetBottom(Ctrl)+2, 2);
		PlaceCtrl(Format); 
		SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH, WW);
		InsertListItem  (Pnl, Ctrl, -1, "Floating point", VAL_FLOATING_PT_FORMAT);
		InsertListItem  (Pnl, Ctrl, -1, "Scientific", 	 VAL_SCIENTIFIC_FORMAT);
		InsertListItem  (Pnl, Ctrl, -1, "Engineering", 	 VAL_ENGINEERING_FORMAT);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CTRL_VAL, Format);
	}
	if (CopyAllow&SRP_PRECISION) {
		CtrlPrecision = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Precision", Top=GetBottom(Ctrl)+2, 2);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE, VAL_INTEGER);
		PlaceCtrl(Precision); 
		SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH, WW);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MIN_VALUE,  0);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MAX_VALUE, 32);
	}
	
	if (CopyAllow&SRP_MARKER_ANGLES) {
		CtrlMarkerStartAngle = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Marker start angle", Top=GetBottom(Ctrl)+2, 2);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE,   VAL_INTEGER);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CHECK_RANGE, VAL_COERCE);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MIN_VALUE, 0);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MAX_VALUE, 359);
		PlaceCtrl(MarkerStartAngle);

		CtrlMarkerEndAngle = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Marker end angle", Top=GetBottom(Ctrl)+2, 2);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE,   VAL_INTEGER);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CHECK_RANGE, VAL_COERCE);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MIN_VALUE, 0);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MAX_VALUE, 359);
		PlaceCtrl(MarkerEndAngle);
	}
	
	if (Top>2) {
		Ctrl = NewCtrl (Pnl, CTRL_HORIZONTAL_SPLITTER_LS, "", Top=GetBottom(Ctrl)+2, 2);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH, Width-4);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CTRL_MODE, VAL_INDICATOR);

		Ctrl = NewCtrl (Pnl, CTRL_TEXT_MSG, "", Top=GetBottom(Ctrl)+2, 2);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CTRL_VAL, "Ramp colors");
	}

	if (CopyAllow&SRP_RAMP) {
		CtrlNb = Ctrl= NewCtrl (Pnl, CTRL_NUMERIC_LS, "Nb of colors", Top=GetBottom(Ctrl)+2, 2);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE,   VAL_INTEGER);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CHECK_RANGE, VAL_COERCE);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MIN_VALUE, 0);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MAX_VALUE, 255);
		PlaceCtrl(NbColors);
	
		CtrlThickness = Ctrl= NewCtrl (Pnl, CTRL_NUMERIC_LS, "Thickness", Top=GetBottom(Ctrl)+2, 2);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE,   VAL_INTEGER);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CHECK_RANGE, VAL_COERCE);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MIN_VALUE, 1);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MAX_VALUE, 10);	// Real limit is 254 but that's a bit much
		PlaceCtrl(Thickness);
	
		CtrlInterpol = Ctrl= NewCtrl (Pnl, CTRL_CHECK_BOX, "Interpolate", Top=GetBottom(Ctrl)+2, 22);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CTRL_VAL, Interpol);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_LABEL_LEFT, W+3);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_LABEL_TOP, Top-1);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CALLBACK_FUNCTION_POINTER, cb_SlideRampChange);

		Top=GetBottom(Ctrl)+2;
		for (i=0; i<255; i++) {
			CtrlColor[i] = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "", Top, 2);
			PlaceCtrl(ColorMapArray[i].color);

			sprintf(Str, "Upper limit %d", i);
			CtrlLimit[i] = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, Str, Top, W+2);
			SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH, WW);
			SetCtrlAttribute(Pnl, Ctrl, ATTR_LABEL_LEFT, W+WW+3);
			SetCtrlAttribute(Pnl, Ctrl, ATTR_LABEL_TOP, Top+4);
			SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE, DataType);
			SetCtrlAttribute(Pnl, Ctrl, ATTR_FORMAT,    Format);
			SetCtrlAttribute(Pnl, Ctrl, ATTR_PRECISION, Precision);
			SetCtrlAttribute(Pnl, Ctrl, ATTR_CTRL_VAL, DataType==VAL_FLOAT
														? ColorMapArray[i].dataValue.valFloat
														: ColorMapArray[i].dataValue.valDouble);
			SetCtrlAttribute(Pnl, Ctrl, ATTR_CALLBACK_FUNCTION_POINTER, cb_SlideRampChange);

			Top=GetBottom(Ctrl);
		}
	
		CtrlColorInf = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Color to +Inf", Top, 2);
		PlaceCtrl(ColorInf);
	}		
	
	cb_SlideRampChange(Pnl, CtrlNb, EVENT_COMMIT, 0, 0, 0);	// Set the initial state

	// Create the buttons
	CtrlOK     = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__OK",     35, 137);
	CtrlCancel = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__Cancel",  5, 137);
	SetCtrlAttribute (Pnl, CtrlCancel,	ATTR_SHORTCUT_KEY, VAL_ESC_VKEY);
	SetCtrlAttribute (Pnl, CtrlOK, 		ATTR_SHORTCUT_KEY, VAL_ENTER_VKEY);
	SetPanelAttribute(Pnl, ATTR_CLOSE_CTRL, CtrlCancel);
	SetCtrlAttribute (Pnl, CtrlOK, 		ATTR_LABEL_BOLD, 1);
	SetCtrlAttribute (Pnl, CtrlOK,      ATTR_WIDTH, 44);
	SetCtrlAttribute (Pnl, CtrlCancel,  ATTR_WIDTH, 44);
	SetCtrlAttribute (Pnl, CtrlOK,      ATTR_CALLBACK_FUNCTION_POINTER, cb_SlideRampPopup);
	SetCtrlAttribute (Pnl, CtrlCancel,  ATTR_CALLBACK_FUNCTION_POINTER, cb_SlideRampPopup);

	// Dimension and position panel
	GetScreenSize(   &ScrHeight,           &ScrWidth);
	SetPanelPos(Pnl, (ScrHeight-Height)/2, (ScrWidth-Width)/2);

	InstallPopup(Pnl);
	Res = RunUserInterface();	// Now we wait until cb_SlideRampPopup calls QuitUserInterface
	
	RemovePopup(0);
	DiscardPanel(Pnl);
	
	if (Res==0) {	// Cancel the changes
		if (CopyAllow&SRP_FILL_COLOR) 		  SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_FILL_COLOR, 			OrigFillColor);
		if (CopyAllow&SRP_FILL_HOUSING_COLOR) SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_FILL_HOUSING_COLOR,	OrigFillHousingColor);
		if (CopyAllow&SRP_SLIDER_COLOR)		  SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_SLIDER_COLOR,		OrigSliderColor);
		if (CopyAllow&SRP_SLIDER_WIDTH)		  SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_SLIDER_WIDTH,		OrigSliderWidth);
		if (CopyAllow&SRP_MIN_VALUE)		  SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_MIN_VALUE,			OrigMinValue);
		if (CopyAllow&SRP_MAX_VALUE)		  SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_MAX_VALUE,			OrigMaxValue);
		if (CopyAllow&SRP_FORMAT)			  SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_FORMAT,				OrigFormat);
		if (CopyAllow&SRP_PRECISION)		  SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_PRECISION,			OrigPrecision);
		if (CopyAllow&SRP_MARKER_ANGLES)	  SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_MARKER_START_ANGLE,	OrigMarkerStartAngle),
											  SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_MARKER_END_ANGLE,	OrigMarkerEndAngle);
		if (CopyAllow&SRP_RAMP) {
			SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_COLOR_RAMP_WIDTH,		OrigThickness);
			SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_COLOR_RAMP_INTERPOLATE,OrigInterpol);
			SetNumericColorRamp (OrigPnl, OrigCtrl, OrigColorMapArray, OrigColorInf, OrigNbColors, OrigInterpol);
		}
	}

	return Res;
}



///////////////////////////////////////////////////////////////////////////////
// Enable the following for testing
///////////////////////////////////////////////////////////////////////////////

#if 0
#include <cvirte.h>
static int CtrlSlide=0, StyleIndex=-1;
	
int CVICALLBACK cb_TestSRP (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK: SlideRampPopup(panel, control, SRP_ALL); break;
	}
	return 0;
}

int CVICALLBACK cb_QuitSRP (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT: QuitUserInterface (0); break;
	}
	return 0;
}

int CVICALLBACK cbp_Change(int panel, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_LEFT_DOUBLE_CLICK:
			if (CtrlSlide!=0) DiscardCtrl (panel, CtrlSlide);
			CtrlSlide = NewCtrl (panel, AuthorizedStyles[(++StyleIndex)%(sizeof(AuthorizedStyles)/4)], "", 50, 80);
			SetCtrlAttribute (panel, CtrlSlide, ATTR_CALLBACK_FUNCTION_POINTER, cb_TestSRP);
			SetCtrlAttribute (panel, CtrlSlide, ATTR_LABEL_TEXT, StrStyles[StyleIndex%(sizeof(AuthorizedStyles)/4)]);

/*			SetBreakOnLibraryErrors (0);
			SetCtrlAttribute (panel, CtrlSlide, ATTR_FILL_COLOR, VAL_GREEN);
			SetCtrlAttribute (panel, CtrlSlide, ATTR_FILL_HOUSING_COLOR, VAL_MAGENTA);
			SetCtrlAttribute (panel, CtrlSlide, ATTR_SLIDER_COLOR, VAL_BLACK);
			SetCtrlAttribute (panel, CtrlSlide, ATTR_SLIDER_WIDTH, 15);
			SetCtrlAttribute (panel, CtrlSlide, ATTR_MARKER_START_ANGLE, 10);
			SetCtrlAttribute (panel, CtrlSlide, ATTR_MARKER_END_ANGLE, 350);
			SetBreakOnLibraryErrors (1);
*/			return 1;	// Otherwise generates a general protection fault
	}
	return 0;
}

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */
	
	int Pnl = NewPanel (0, "Test slide popup", 100, 100, 300, 300);

	int CtrlQuit= NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "QUIT", 0, 0);
	SetCtrlAttribute (Pnl, CtrlQuit, ATTR_VISIBLE, 0);
	SetCtrlAttribute (Pnl, CtrlQuit, ATTR_CALLBACK_FUNCTION_POINTER, cb_QuitSRP);
	SetPanelAttribute(Pnl, ATTR_CLOSE_CTRL, CtrlQuit);
	SetPanelAttribute(Pnl, ATTR_CALLBACK_FUNCTION_POINTER, cbp_Change);
	
	cbp_Change(Pnl, EVENT_LEFT_DOUBLE_CLICK, NULL, 0, 0);
	
	DisplayPanel(Pnl);
	RunUserInterface();
	return 0;
}

#endif

