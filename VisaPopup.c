#include "toolbox.h"
#include <userint.h>
#include "VisaPopup.h"

#define Margin 2		// 2 pixels around elements
#define MAX(a,b) ((a)>=(b)?(a):(b))

static int Pnl=0;
static int CtrlSelect=0, CtrlDescr=0, CtrlCancel=0;

/// HIFN	Callback function of the buttons
static int CVICALLBACK cb_VisaPopup (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface(control!=CtrlSelect);
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Displays a list of visa instrument descriptors in a listbox
/// HIFN	and waits for the user to select one (or cancel)
/// HIFN	Uses viFindRsrc to list resources and offer the selection in a popup in a similar way to NIvisaic.exe
/// HIPAR	Rsrc/Session to a Resource Manager returned from viOpenDefaultRM.
/// HIPAR	Expression/Matches the value specified in Expression with all known instrument
/// HIPAR	Expression/Default is "?*INSTR"
/// HIPAR	Descr/The descriptor string for the selected instrument to use with viOpen. *Descr==0 is user canceled
/// HIPAR	Message/Additional message to display in the popup
/// HIRET	0 is selected valid Descr, 1 if cancel
/// HIRET	Negative values indicate an error. See QuitUserInterface
///////////////////////////////////////////////////////////////////////////////
int VisaPopup(ViSession Rsrc, ViString Expression, char* Descr,
			  const char* Title, const char* Message) {
	int Count=0;
	*Descr=0;

	int CtrlText, TxtWidth, TxtHeight, ScrWidth, ScrHeight, Height, Width;
	int Left=Margin, CtrlHeight, CtrlWidth;

	ViFindList FindHandle;
	int SBOLE=SetBreakOnLibraryErrors(0);
	int St = viFindRsrc (Rsrc, Expression, &FindHandle, &Count, Descr);
	SetBreakOnLibraryErrors(SBOLE);
		if (St) return St;	// Nothing found, so just return negative value and empty Descr without even asking the user
	if (Count==1) return 0;	// If only one is found, just return it

	// Create the panel
	Pnl = NewPanel (0, Title, 0, 0, 400, 400);
	CtrlText = NewCtrl (Pnl, CTRL_TEXT_MSG, Message, Margin, Margin);
	GetCtrlAttribute(Pnl, CtrlText, ATTR_WIDTH,  &TxtWidth);
	GetCtrlAttribute(Pnl, CtrlText, ATTR_HEIGHT, &TxtHeight);

	// Create the listbox, fill it up with found resource descriptors
	CtrlDescr = NewCtrl (Pnl, CTRL_RECESSED_MENU_RING_LS, "__Descriptors",
						 TxtHeight+2*Margin+10, Left);
	SetCtrlAttribute(Pnl, CtrlDescr, ATTR_LABEL_TEXT, "");
	SetCtrlAttribute(Pnl, CtrlDescr, ATTR_WIDTH,  TxtWidth-2*Margin);
	GetCtrlAttribute(Pnl, CtrlDescr, ATTR_HEIGHT, &CtrlHeight);
	InsertListItem  (Pnl, CtrlDescr, -1, Descr, 0);	// Values aren't used, only labels
	for (int i=1; i<Count; i++) {
		St=viFindNext (FindHandle, Descr);
		InsertListItem (Pnl, CtrlDescr, -1, Descr, i);
	}

	// Create the select and cancel buttons
	CtrlSelect = NewCtrl (Pnl, CTRL_SQUARE_COMMAND_BUTTON_LS, "__Select", TxtHeight+2*CtrlHeight+2*Margin, Left);
	GetCtrlAttribute(Pnl, CtrlSelect, ATTR_WIDTH, &CtrlWidth); Left+=CtrlWidth+Margin;
	SetCtrlAttribute(Pnl, CtrlSelect, ATTR_SHORTCUT_KEY, VAL_ENTER_VKEY);

	CtrlCancel = NewCtrl (Pnl, CTRL_SQUARE_COMMAND_BUTTON_LS, "__Cancel", TxtHeight+2*CtrlHeight+2*Margin, Left);
	GetCtrlAttribute (Pnl, CtrlCancel, ATTR_WIDTH, &CtrlWidth); Left+=CtrlWidth+Margin;
	SetCtrlAttribute (Pnl, CtrlCancel, ATTR_SHORTCUT_KEY, VAL_ESC_VKEY);
	SetPanelAttribute(Pnl, ATTR_CLOSE_CTRL, CtrlCancel);

	// Dimension and position panel. That's approximate
	SetPanelSize(Pnl, Height=TxtHeight+3*Margin+3*CtrlHeight+10,
					  Width=MAX(TxtWidth+2*Margin, Left));
	GetScreenSize (&ScrHeight, &ScrWidth);
	SetPanelPos (Pnl, ScrHeight/2-Height/2, ScrWidth/2-Width/2);

	SetCtrlAttribute (Pnl, CtrlSelect, ATTR_CALLBACK_FUNCTION_POINTER, cb_VisaPopup);
	SetCtrlAttribute (Pnl, CtrlCancel, ATTR_CALLBACK_FUNCTION_POINTER, cb_VisaPopup);

	InstallPopup(Pnl);

	int Res = RunUserInterface();
	if (Res==0) {	// Read the selected descriptor from the control
		int Index=0;
		GetCtrlIndex     (Pnl, CtrlDescr, &Index);
		GetLabelFromIndex(Pnl, CtrlDescr,  Index, Descr);
	} else *Descr=0;

	RemovePopup (0);
	DiscardPanel(Pnl);
	return Res;
}
