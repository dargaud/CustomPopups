#ifndef _PRECISION_POPUP
#define _PRECISION_POPUP

// Allow the change of those
#define PP_FORMAT        (1<<0)
#define PP_PRECISION     (1<<1)
#define PP_PADDING       (1<<2)
#define PP_INCR_VALUE    (1<<3)
#define PP_MIN_VALUE	 (1<<4)
#define PP_MAX_VALUE	 (1<<5)
#define PP_LABEL_COLOR   (1<<6)
#define PP_LABEL_BGCOLOR (1<<7)
#define PP_TEXT_COLOR	 (1<<8)
#define PP_TEXT_BGCOLOR	 (1<<9)
#define PP_FRAME_COLOR	 (1<<10)
// Shortcuts
#define PP_NUMERICS      (PP_LABEL_COLOR-1)		// All numerical properties
#define PP_COLORS        (PP_LABEL_COLOR|PP_LABEL_BGCOLOR|PP_TEXT_COLOR|PP_TEXT_BGCOLOR|PP_FRAME_COLOR)
#define PP_ALL			 (PP_NUMERICS|PP_LABEL_COLOR)

// See .c file for description
extern int PrecisionPopup(int OrigPnl, int OrigCtrl, int Allow);

#endif
