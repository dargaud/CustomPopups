#ifndef _FULL_CONFIRM_POPUP
#define _FULL_CONFIRM_POPUP

#define VAL_POPUP_YES 1
#define VAL_POPUP_YES_TO_ALL 2
#define VAL_POPUP_NO 3
#define VAL_POPUP_NO_TO_ALL 4
#define VAL_POPUP_CANCEL 5

///////////////////////////////////////////////////////////////////////////////
// FUNCTION: FullConfirmPopup
// PURPOSE: Displays a prompt message in a dialog box and waits for the user
// 			to select the 'Yes', 'Yes to All', 'No', 'No to All' or 'Cancel' button.
// IN: Title: The title to be displayed on the dialog box.
//     Message: The message to be displayed in the dialog box.
//              To display a multi-line message, embed newline characters (\n) in the message string.
// RETURN: Contains the user's response:
//				VAL_POPUP_YES 		- User selected 'Yes'
//				VAL_POPUP_YES_TO_ALL- User selected 'Yes To All'
//				VAL_POPUP_NO		- User selected 'No'
//				VAL_POPUP_NO_TO_ALL	- User selected 'No To All'
//				VAL_POPUP_CANCEL	- User selected 'Cancel'
//			Negative values indicate an error. See QuitUserInterface
///////////////////////////////////////////////////////////////////////////////
extern int FullConfirmPopup(const char* Title, const char* Message);

#endif
