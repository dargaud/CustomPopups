#ifndef _EDIT_AXIS_SETTINGS_POPUP
#define _EDIT_AXIS_SETTINGS_POPUP

// NOTE 1: a lot more are possible, see ATTR_X*:
// ATTR_XYNAME_FONT, ATTR_XYNAME_CHARACTER_SET, ATTR_XYNAME_COLOR, ATTR_XYLABEL_FONT, ATTR_XYLABEL_COLOR
// ATTR_XNAME, ATTR_XGRID_VISIBLE, ATTR_XLABEL_VISIBLE, ATTR_XFORMAT, ATTR_XDIVISIONS
// ATTR_XPRECISION, ATTR_XENG_UNITS, ATTR_XYNAME_BOLD, ATTR_XYNAME_ITALIC, ATTR_XYNAME_UNDERLINE
// ATTR_XYNAME_STRIKEOUT, ATTR_XYNAME_POINT_SIZE, ATTR_XNAME_LENGTH, ATTR_XYNAME_FONT_NAME_LENGTH
// ATTR_XYLABEL_BOLD, ATTR_XYLABEL_ITALIC, ATTR_XYLABEL_UNDERLINE, ATTR_XYLABEL_STRIKEOUT
// ATTR_XYLABEL_POINT_SIZE, ATTR_XYLABEL_FONT_NAME_LENGTH, ATTR_XUSE_LABEL_STRINGS, ATTR_XAXIS_GAIN
// ATTR_XAXIS_OFFSET, ATTR_XPADDING, ATTR_XMINORGRID_VISIBLE, ATTR_XMAP_MODE, ATTR_XMARK_ORIGIN
// ATTR_XREVERSE, ATTR_XLOOSE_FIT_AUTOSCALING, ATTR_XLOOSE_FIT_AUTOSCALING_UNIT, ATTR_XRESOLUTION
// ATTR_XOFFSET, ATTR_XCOORD_AT_ORIGIN, ATTR_XSCALING, ATTR_XLABEL_FONT, ATTR_XLABEL_COLOR
// ATTR_XLABEL_BOLD, ATTR_XLABEL_ANGLE, ATTR_XLABEL_ITALIC, ATTR_XLABEL_UNDERLINE
// ATTR_XLABEL_STRIKEOUT, ATTR_XLABEL_POINT_SIZE, ATTR_XLABEL_FONT_NAME_LENGTH, ATTR_XLABEL_CHARACTER_SET
// And the same for ATTR_Y*...
// And that's not even counting some global graph properties that impact axes such as
// ATTR_ENABLE_EDITABLE_AXES, ATTR_ZOOM_STYLE...

// NOTE 2: eventually it would be nice to have the same aspect for the popup
//         as the axis setting of the user interface editor


// Allow the change of those
#define EASP_FORMAT		(1<<0)
#define EASP_PRECISION	(1<<1)
//#define EASP_PADDING	(1<<2)
#define EASP_LOG	 	(1<<3)
#define EASP_GRID		(1<<4)
#define EASP_MARK		(1<<5)
#define EASP_REVERSE	(1<<6)
#define EASP_NAME		(1<<7)
#define EASP_SCALING	(1<<8)

// Shortcut for all of them
#define EASP_ALL (EASP_FORMAT|EASP_PRECISION|EASP_LOG |EASP_GRID   | \
				  EASP_MARK  |EASP_REVERSE  |EASP_NAME|EASP_SCALING)

// See .c file for description
extern int EditAxisSettingsPopup(int OrigPnl, int OrigCtrl, int Axis, int Allow);

extern int ChangeAxisProperties(int panel, int control, int eventData1, int eventData2);

#endif
