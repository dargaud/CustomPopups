///////////////////////////////////////////////////////////////////////////////
// MODULE	PrecisionPopup
// PURPOSE	Presents a popup that allows changing a few options of a numerical control
//			The precision, the numerical format, the padding and the inc arrow step
// LIMITATION	Currently only works on VAL_DOUBLE and VAL_FLOAT types of numerics
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <userint.h>
#include <iso646.h>

#include "PrecisionPopup.h"

#define Margin 5		// 2 pixels around elements
#define Step 50			// Number of pixels between each line of controls
#define MAX(a,b) ((a)>=(b)?(a):(b))

// Temporary panel and controls created for the popup
static int Pnl=0;
static int CtrlOK=0,  CtrlCancel=0, 
		CtrlFormat=0, CtrlPrecision=0, CtrlPadding=0, CtrlIncrVal=0, CtrlMinVal=0, CtrlMaxVal=0,
		CtrlSample=0, CtrlDeco=0,
		CtrlLabelColor=0,  CtrlLabelBgColor=0, CtrlTextColor=0,  CtrlTextBgColor=0, CtrlFrameColor=0;

// Characteristics
static int Format, Precision, Padding, 
			LabelColor, LabelBgColor, TextColor, TextBgColor, FrameColor;
static double IncrVal, MinVal, MaxVal;

// Which ones are user settable
static int CopyAllow=0;

///////////////////////////////////////////////////////////////////////////////
// Callback function to update the sample
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_SamplePopup (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			if (control==CtrlCancel) { QuitUserInterface(0); break; }
			if (control==CtrlOK)     { QuitUserInterface(1); break; }

			if (CopyAllow & PP_FORMAT) {
				GetCtrlVal      (Pnl, CtrlFormat,             &Format);
				SetCtrlAttribute(Pnl, CtrlSample, ATTR_FORMAT, Format);
				if (CtrlIncrVal) SetCtrlAttribute(Pnl, CtrlIncrVal,ATTR_FORMAT, Format);
			}

			if (CopyAllow & PP_PRECISION) {
				GetCtrlVal      (Pnl, CtrlPrecision,             &Precision);
				SetCtrlAttribute(Pnl, CtrlSample, ATTR_PRECISION, Precision);
				if (CtrlIncrVal) SetCtrlAttribute(Pnl, CtrlIncrVal,ATTR_PRECISION, Precision);
			}
			
		#define CHECK(WHAT, What) \
			if (CopyAllow & PP_##WHAT) \
				GetCtrlVal      (Pnl, Ctrl##What,             &What),\
				SetCtrlAttribute(Pnl, CtrlSample, ATTR_##WHAT, What)
				
			CHECK(PADDING,		Padding);	
			CHECK(INCR_VALUE,	IncrVal);
			CHECK(MIN_VALUE,	MinVal);
			CHECK(MAX_VALUE,	MaxVal);
			CHECK(LABEL_COLOR,	LabelColor);
			CHECK(LABEL_BGCOLOR,LabelBgColor);
			CHECK(TEXT_COLOR,	TextColor);
			CHECK(TEXT_BGCOLOR,	TextBgColor);
			CHECK(FRAME_COLOR,	FrameColor);
			break;
	}
	panel=eventData1=eventData2=0; callbackData=NULL;	// Keeps warning away
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
// HIFN	 	Displays a prompt message in a dialog box and waits for the user
//			to change the precision and/or format type for a numeric control
// HIPAR	OrigPnl/Panel where the control is located
// HIPAR	OrigCtrl/Numeric control to change. Some numeric types may have limitations
// HIPAR	Allow/Bitfield of which attributes to present for change
// HIRET	1 if either has been changed, 0 if none, <0 if error (standard NI error code)
///////////////////////////////////////////////////////////////////////////////
int PrecisionPopup(int OrigPnl, int OrigCtrl, int Allow) {
	int Ctrl, CtrlText, MsgHeight, ScrWidth, ScrHeight, Height, Width;
	int Res, Err, Type, L1, L2;
	char Str[255], Label[255];
	double Val;
	float F;
	int Style, DataType, Int;
	CopyAllow=Allow;
	
	CtrlOK=CtrlCancel=CtrlSample=CtrlDeco=
	CtrlFormat=CtrlPrecision=CtrlPadding=CtrlIncrVal=CtrlMinVal=CtrlMaxVal=
	CtrlLabelColor=CtrlLabelBgColor=CtrlTextColor=CtrlTextBgColor=CtrlFrameColor=0;

#define GET(WHAT, What) \
		Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_##WHAT,  &What);\
		if (Err<0) return Err	// Probably wrong type of control

	// Sanity checks (we could extend this to VAL_FLOAT)
	if (Allow&PP_FORMAT or Allow&PP_PRECISION or Allow&PP_PADDING or 
		Allow&PP_INCR_VALUE or Allow&PP_MIN_VALUE or Allow&PP_MAX_VALUE) {
		Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_DATA_TYPE,    &Type);
		if (Err<0) return Err;	// Probably wrong type of control
		if (Type!=VAL_DOUBLE and Type!=VAL_FLOAT) return UIEInvalidControlType;
	
		GET(FORMAT, Format);
		if (Format!=VAL_FLOATING_PT_FORMAT and Format!=VAL_SCIENTIFIC_FORMAT and Format!=VAL_ENGINEERING_FORMAT)
			return UIEControlNotTypeExpectedByFunction;
	
		// Check that the parameters we want to change are indeed available
		GET(PRECISION, Precision);
		if (Precision<0 or 32<Precision) return UIEValueIsInvalidOrOutOfRange;

		GET(PADDING, Padding);
		if (Padding<0 or 64<Padding) return UIEValueIsInvalidOrOutOfRange;

#define GETF(WHAT, What) \
		if (Type==VAL_DOUBLE) Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_##WHAT, &What);\
		else { Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_##WHAT, &F); What=F; }\
		if (Err<0) return Err	// Probably wrong type of control

		GETF(INCR_VALUE, IncrVal);
		GETF(MIN_VALUE,  MinVal);
		GETF(MAX_VALUE,  MaxVal);
	} else IncrVal=MinVal=MaxVal=Type=Format=Precision=Padding=0;
	
	GET(LABEL_COLOR,   LabelColor);
	GET(LABEL_BGCOLOR, LabelBgColor);
	GET(TEXT_COLOR,    TextColor);
	GET(TEXT_BGCOLOR,  TextBgColor);
	GET(FRAME_COLOR,   FrameColor);
		
	// Create the panel
	Pnl = NewPanel (0, "Precision change popup", 0, 0, Height=Step*4-10, Width=600);
	
	GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_LABEL_TEXT, Label);
	sprintf(Str, "Set attributes for control '%s'", Label);
	CtrlText      = NewCtrl (Pnl, CTRL_TEXT_MSG, Str, Margin, Margin);
	GetCtrlAttribute(Pnl, CtrlText, ATTR_HEIGHT, &MsgHeight);
	
	// Create the inputs and the sample
	L1=L2=Margin-100;
	if (Allow&PP_FORMAT) {
		CtrlFormat    = Ctrl = NewCtrl (Pnl, CTRL_RING_LS,    "Format", Step, L1+=100);
		InsertListItem (Pnl, Ctrl, -1, "Floating point",VAL_FLOATING_PT_FORMAT);
		InsertListItem (Pnl, Ctrl, -1, "Scientific",	  VAL_SCIENTIFIC_FORMAT);
		InsertListItem (Pnl, Ctrl, -1, "Engineering",	  VAL_ENGINEERING_FORMAT);
		SetCtrlVal(Pnl, Ctrl,    Format);
	}
	
	if (Allow&PP_PRECISION) {
		CtrlPrecision = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Precision", Step, L1+=100);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE,   VAL_INTEGER);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CHECK_RANGE, VAL_COERCE);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MIN_VALUE, 0);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MAX_VALUE, 32);
		SetCtrlVal(Pnl, Ctrl, Precision);
	}
	
	if (Allow&PP_PADDING) {
		CtrlPadding   = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Padding", Step, L1+=100);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_DATA_TYPE,   VAL_INTEGER);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_CHECK_RANGE, VAL_COERCE);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MIN_VALUE, 0);
		SetCtrlAttribute(Pnl, Ctrl, ATTR_MAX_VALUE, 64);
		SetCtrlVal(Pnl, Ctrl, Padding);
	}
	
	if (Allow&PP_INCR_VALUE) {
		CtrlIncrVal       = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Increment arrows", Step, L1+=100);
		SetCtrlVal(Pnl, Ctrl, IncrVal);
	}

	if (Allow&PP_MIN_VALUE) {
		CtrlMinVal       = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Min value", Step, L1+=100);
		SetCtrlVal(Pnl, Ctrl, MinVal);
	}

	if (Allow&PP_MAX_VALUE) {
		CtrlMaxVal       = Ctrl = NewCtrl (Pnl, CTRL_NUMERIC_LS, "Max value", Step, L1+=100);
		SetCtrlVal(Pnl, Ctrl, MaxVal);
	}
	
	if (Allow&PP_LABEL_COLOR) {
		CtrlLabelColor    = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Label color", 2*Step, L2+=100);
		SetCtrlVal(Pnl, Ctrl, LabelColor);
	}
	
	if (Allow&PP_LABEL_BGCOLOR) {
		CtrlLabelBgColor  = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Label background", 2*Step, L2+=100);
		SetCtrlVal(Pnl, Ctrl, LabelBgColor);
	}

	if (Allow&PP_TEXT_COLOR) {
		CtrlTextColor    = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Text color", 2*Step, L2+=100);
		SetCtrlVal(Pnl, Ctrl, TextColor);
	}

	if (Allow&PP_TEXT_BGCOLOR) {
		CtrlTextBgColor  = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Text background", 2*Step, L2+=100);
		SetCtrlVal(Pnl, Ctrl, TextBgColor);
	}

	if (Allow&PP_FRAME_COLOR) {
		CtrlFrameColor    = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Frame color", 2*Step, L2+=100);
		SetCtrlVal(Pnl, Ctrl, FrameColor);
	}
	
	CtrlDeco      = Ctrl = NewCtrl (Pnl, CTRL_RAISED_BOX_LS, 0, 3*Step-25, 0);
	SetCtrlAttribute(Pnl, Ctrl, ATTR_WIDTH, 100-Margin);
	SetCtrlAttribute(Pnl, Ctrl, ATTR_HEIGHT, Step);
	
	GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_CTRL_STYLE, &Style);
	GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_DATA_TYPE,  &DataType);
	CtrlSample    = Ctrl = NewCtrl (Pnl, Style, "Sample", 3*Step, Margin);
	SetCtrlAttribute(Pnl, Ctrl, ATTR_CHECK_RANGE, VAL_NOTIFY);
	switch (DataType) {
		case VAL_DOUBLE: GetCtrlVal(OrigPnl, OrigCtrl, &Val);
						 SetCtrlVal(Pnl,     Ctrl,      Val);
						 break;
		case VAL_FLOAT:  GetCtrlVal(OrigPnl, OrigCtrl,    &F);
						 SetCtrlVal(Pnl,     Ctrl, (double)F);
						 break;
		case VAL_INTEGER:GetCtrlVal(OrigPnl, OrigCtrl, &Int);
						 SetCtrlVal(Pnl,     Ctrl,      Int);
						 break;
	}
	
	// Create the buttons
	CtrlOK     = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__OK",     3*Step, 150);
	CtrlCancel = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__Cancel", 3*Step, 100);
	SetCtrlAttribute (Pnl, CtrlCancel, ATTR_SHORTCUT_KEY, VAL_ESC_VKEY);
	SetPanelAttribute(Pnl, ATTR_CLOSE_CTRL, CtrlCancel);

	cb_SamplePopup(0, 0, EVENT_COMMIT, 0, 0, 0);	// Set the initial state

	// Dimension and position panel
	if (L1<0 and L2<0) { DiscardPanel(Pnl); return UIEValueIsInvalidOrOutOfRange; }
	if (L1<200+Margin) L1=200+Margin;
	if (L2<200+Margin) L2=200+Margin;
	SetPanelAttribute(Pnl, ATTR_WIDTH, Width=MAX(L1,L2)+100);
	GetScreenSize (&ScrHeight, &ScrWidth);
	SetPanelPos (Pnl, (ScrHeight-Height)/2, (ScrWidth-Width)/2);
	
	// Can't use SetAttributeForCtrls() here
#define SETFP(Ctrl) if ((Ctrl)!=0) SetCtrlAttribute(Pnl, (Ctrl), ATTR_CALLBACK_FUNCTION_POINTER, cb_SamplePopup)
	SETFP(CtrlOK);
	SETFP(CtrlCancel);
	SETFP(CtrlPrecision);
	SETFP(CtrlFormat);
	SETFP(CtrlPadding);
	SETFP(CtrlIncrVal);
	SETFP(CtrlMinVal);
	SETFP(CtrlMaxVal);
	SETFP(CtrlLabelColor);
	SETFP(CtrlLabelBgColor);
	SETFP(CtrlTextColor);
	SETFP(CtrlTextBgColor);
	SETFP(CtrlFrameColor);

	InstallPopup(Pnl);

	Res = RunUserInterface();	// Now we wait until cb_PrecisionPopup calls QuitUserInterface
	
	RemovePopup(0);
	DiscardPanel(Pnl);
	
	if (Res==1) {	// Commit the changes
		if (Allow&PP_FORMAT)        SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_FORMAT,       Format);
		if (Allow&PP_PADDING)       SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_PADDING,      Padding);
		if (Allow&PP_PRECISION)     SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_PRECISION,    Precision);
		if (Allow&PP_INCR_VALUE)    SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_INCR_VALUE,   Type==VAL_DOUBLE?IncrVal:(float)IncrVal);
		if (Allow&PP_MIN_VALUE)     SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_MIN_VALUE,    Type==VAL_DOUBLE?MinVal :(float)MinVal);
		if (Allow&PP_MAX_VALUE)     SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_MAX_VALUE,    Type==VAL_DOUBLE?MaxVal :(float)MaxVal);
		if (Allow&PP_LABEL_COLOR)   SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_LABEL_COLOR,  LabelColor);
		if (Allow&PP_LABEL_BGCOLOR) SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_LABEL_BGCOLOR,LabelBgColor);
		if (Allow&PP_TEXT_COLOR)    SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_TEXT_COLOR,   TextColor);
		if (Allow&PP_TEXT_BGCOLOR)  SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_TEXT_BGCOLOR, TextBgColor);
		if (Allow&PP_FRAME_COLOR)   SetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_FRAME_COLOR,  FrameColor);
	}

	return Res;
}


#if 0
#include <stdlib.h>
#include <cvirte.h>
	
int CVICALLBACK cb_TestPP (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK: PrecisionPopup(panel, control, 
									//PP_ALL
									rand()	// RAND_MAX is 32767 so all bits are covered
								); 
								break;
	}
	return 0;
}

int CVICALLBACK cb_QuitPP (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT: QuitUserInterface (0); break;
	}
	return 0;
}

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */
	
	int Pnl = NewPanel (0, "Test precision popup", 100, 100, 300, 300);

	int CtrlQuit= NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "QUIT", 0, 0);
	SetCtrlAttribute (Pnl, CtrlQuit, ATTR_VISIBLE, 0);
	SetCtrlAttribute (Pnl, CtrlQuit, ATTR_CALLBACK_FUNCTION_POINTER, cb_QuitPP);
	SetPanelAttribute(Pnl, ATTR_CLOSE_CTRL, CtrlQuit);
	
	SetCtrlAttribute (Pnl, NewCtrl (Pnl, CTRL_NUMERIC_LS,      "LS numeric",   20,  2), ATTR_CALLBACK_FUNCTION_POINTER, cb_TestPP);
	SetCtrlAttribute (Pnl, NewCtrl (Pnl, CTRL_NUMERIC_KNOB_LS, "Knob numeric", 90, 40), ATTR_CALLBACK_FUNCTION_POINTER, cb_TestPP);
	
	DisplayPanel(Pnl);
	RunUserInterface();
	return 0;
}

#endif

