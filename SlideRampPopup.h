#ifndef _SLIDE_RAMP_POPUP
#define _SLIDE_RAMP_POPUP

// Allow the change of those
#define SRP_RAMP				(1<<0)	// Allows change of the various ramp attributes: 
										//	ATTR_COLOR_RAMP_WIDTH, ATTR_COLOR_RAMP_INTERPOLATE 
										//	and the color map array
#define SRP_FILL_COLOR			(1<<1)  // Allows change of ATTR_FILL_COLOR
#define SRP_FILL_HOUSING_COLOR	(1<<2)	// Allows change of ATTR_FILL_HOUSING_COLOR
#define SRP_SLIDER_COLOR		(1<<3)	// Allows change of ATTR_SLIDER_COLOR
#define SRP_SLIDER_WIDTH		(1<<4)	// Allows change of ATTR__SLIDER_WIDTH
#define SRP_MARKER_ANGLES		(1<<5)	// Allows change of ATTR_MARKER_START_ANGLE and ATTR_MARKER_END_ANGLE
#define SRP_MIN_VALUE			(1<<6)	// Allows change of ATTR_MIN_VALUE
#define SRP_MAX_VALUE			(1<<7)	// Allows change of ATTR_MAX_VALUE
#define SRP_FORMAT				(1<<8)	// Allows change of ATTR_FORMAT
#define SRP_PRECISION			(1<<9)	// Allows change of ATTR_PRECISION
#define SRP_ALL				   ((1<<10)-1)	// All of the above

// See .c file for description
extern int SlideRampPopup(int OrigPnl, int OrigCtrl, int Allow);

#endif
