# Various custom popup panels for LabWindows/CVI:

 - Full confirmation popup (yes/no/yes all/no all/cancel)
 - numeric precision popup
 - edit axis settings popup
 - slider ramp settings popup
 - LED on/off colors popup
 - digital image filter popup
 - VISA instrument selector popup...

See http://www.gdargaud.net/Hack/LabWindows.html#Popup for explanation and examples.

See also the header files on how to use in LabWindows; they all work in similar ways.
