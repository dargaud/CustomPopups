///////////////////////////////////////////////////////////////////////////////
// MODULE	ColorsOnOffPopup
// PURPOSE	Presents a popup that allows changing a the ON and OFF color of LEDs and buttons
// AUTHOR	Guillaume Dargaud 
// (c)		GPL
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <userint.h>
#include <utility.h>
#include <iso646.h>

#include "ColorsOnOffPopup.h"

#define Margin 2		// 2 pixels around elements
#define Step 50			// Number of pixels between each line of controls
#define MAX(a,b) ((a)>=(b)?(a):(b))


// Temporary panel and controls created for the popup
static int Pnl=0, OrigPnl_, OrigCtrl_;

static int CtrlOK=0,  CtrlCancel=0, CtrlOnColor=0, CtrlOffColor=0;

// Originals for cancel
static int OrigOnColor, OrigOffColor;

///////////////////////////////////////////////////////////////////////////////
// Callback function of the buttons, quit with or without committing the results
///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_ColorOnOffPopup (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int OnColor, OffColor;
	switch (event) {
		case EVENT_COMMIT:
			if (control==CtrlCancel) QuitUserInterface(0);
			if (control==CtrlOK)     QuitUserInterface(1); 

			#define CheckCtrl(C, c)\
				if (control==Ctrl##c) {\
					GetCtrlVal(panel, control, &c);\
					SetCtrlAttribute(OrigPnl_, OrigCtrl_, ATTR_##C, c);\
					break;\
				}
			
			CheckCtrl(ON_COLOR,  OnColor);
			CheckCtrl(OFF_COLOR, OffColor);
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Return the bottom position of a control
/// HIRET	Sum of Top+Height
///////////////////////////////////////////////////////////////////////////////
static int GetBottom(int Ctrl) {
	int Top, Height;
	if (Ctrl==0) return 0;
	GetCtrlAttribute(Pnl, Ctrl, ATTR_TOP,    &Top);
	GetCtrlAttribute(Pnl, Ctrl, ATTR_HEIGHT, &Height);
	return Top+Height;
}


///////////////////////////////////////////////////////////////////////////////
// HIFN	 	Displays a prompt message in a dialog box and waits for the user
//			to change the characteristics of a LED/button
// HIPAR	OrigPnl/Panel where the control is located
// HIPAR	OrigCtrl/Numeric meter control to change.
// HIRET	1 if either has been changed, 0 if none, <0 if error (standard NI error code)
///////////////////////////////////////////////////////////////////////////////
int ColorsOnOffPopup(int OrigPnl, int OrigCtrl) {
	int Ctrl, ScrWidth, ScrHeight, Height, Width, W;
	int Res, Err, Top=2, SBLE;
	
	OrigPnl_ =OrigPnl;
	OrigCtrl_=OrigCtrl;
	
	CtrlOK=CtrlCancel=CtrlOnColor=CtrlOffColor=0;

	SBLE = SetBreakOnLibraryErrors(0);
	if ((Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_ON_COLOR,  &OrigOnColor ))<0 or
		(Err=GetCtrlAttribute (OrigPnl, OrigCtrl, ATTR_OFF_COLOR, &OrigOffColor))<0) return Err;
	SetBreakOnLibraryErrors(SBLE);
	
	
	// Create the panel
	Pnl = NewPanel (0, "On/off colors", 0, 0, Height=80, Width=90);
	Ctrl=0;

	// Create the input controls
	CtrlOnColor  = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "On Color",  Top=GetBottom(Ctrl)+2, 2);
	SetCtrlAttribute (Pnl, Ctrl, ATTR_LABEL_TOP, Top+2);
	CtrlOffColor = Ctrl = NewCtrl (Pnl, CTRL_COLOR_NUMERIC_LS, "Off Color", Top=GetBottom(Ctrl)+2, 2);
	SetCtrlAttribute (Pnl, Ctrl, ATTR_LABEL_TOP, Top+2);
	
	SetCtrlAttribute(Pnl, CtrlOnColor,  ATTR_CTRL_VAL, OrigOnColor);
	SetCtrlAttribute(Pnl, CtrlOffColor, ATTR_CTRL_VAL, OrigOffColor);
	
	SetCtrlAttribute(Pnl, CtrlOnColor,  ATTR_CALLBACK_FUNCTION_POINTER, cb_ColorOnOffPopup);
	SetCtrlAttribute(Pnl, CtrlOffColor, ATTR_CALLBACK_FUNCTION_POINTER, cb_ColorOnOffPopup);
	
	GetCtrlAttribute(Pnl, CtrlOnColor,  ATTR_HEIGHT, &W);
	SetCtrlAttribute(Pnl, CtrlOnColor,  ATTR_WIDTH, W);
	SetCtrlAttribute(Pnl, CtrlOffColor, ATTR_WIDTH, W);
	SetCtrlAttribute(Pnl, CtrlOnColor,  ATTR_LABEL_LEFT, W+5);
	SetCtrlAttribute(Pnl, CtrlOffColor, ATTR_LABEL_LEFT, W+5);

	// Create the buttons
	CtrlOK     = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__OK",     Top=GetBottom(Ctrl)+2, 50);
	CtrlCancel = NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "__Cancel", Top, 2);
	SetCtrlAttribute (Pnl, CtrlCancel,	ATTR_SHORTCUT_KEY, VAL_ESC_VKEY);
	SetCtrlAttribute (Pnl, CtrlOK, 		ATTR_SHORTCUT_KEY, VAL_ENTER_VKEY);
	SetPanelAttribute(Pnl, ATTR_CLOSE_CTRL, CtrlCancel);
	SetCtrlAttribute (Pnl, CtrlOK, 		ATTR_LABEL_BOLD, 1);
	SetCtrlAttribute (Pnl, CtrlOK,      ATTR_WIDTH, 44);
	SetCtrlAttribute (Pnl, CtrlCancel,  ATTR_WIDTH, 44);
	SetCtrlAttribute (Pnl, CtrlOK,      ATTR_CALLBACK_FUNCTION_POINTER, cb_ColorOnOffPopup);
	SetCtrlAttribute (Pnl, CtrlCancel,  ATTR_CALLBACK_FUNCTION_POINTER, cb_ColorOnOffPopup);

	// Dimension and position panel
	GetScreenSize(   &ScrHeight,           &ScrWidth);
	SetPanelPos(Pnl, (ScrHeight-Height)/2, (ScrWidth-Width)/2);

	InstallPopup(Pnl);
	Res = RunUserInterface();	// Now we wait until cb_ColorsOnOffPopup calls QuitUserInterface
	
	RemovePopup(0);
	DiscardPanel(Pnl);
	
	if (Res==0) {	// Cancel the changes
		SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_ON_COLOR, 	OrigOnColor);
		SetCtrlAttribute(OrigPnl, OrigCtrl, ATTR_OFF_COLOR,	OrigOffColor);
	}

	return Res;
}

#if 0
#include <cvirte.h>
	
int CVICALLBACK cb_TestSRP (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK: ColorsOnOffPopup(panel, control); break;
	}
	return 0;
}

int CVICALLBACK cb_QuitSRP (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT: QuitUserInterface (0); break;
	}
	return 0;
}

int main (int argc, char *argv[]) {
	int Ctrl;
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;    /* out of memory */
	
	int Pnl = NewPanel (0, "Test colors on/off popup", 100, 100, 300, 300);

	int CtrlQuit= NewCtrl (Pnl, CTRL_ROUNDED_COMMAND_BUTTON, "QUIT", 0, 0);
	SetCtrlAttribute (Pnl, CtrlQuit, ATTR_VISIBLE, 0);
	SetCtrlAttribute (Pnl, CtrlQuit, ATTR_CALLBACK_FUNCTION_POINTER, cb_QuitSRP);
	SetPanelAttribute(Pnl, ATTR_CLOSE_CTRL, CtrlQuit);
	
	Ctrl=NewCtrl(Pnl, CTRL_SQUARE_BUTTON_LS, "Square button", 20, 30);
	SetCtrlAttribute (Pnl, Ctrl, ATTR_CALLBACK_FUNCTION_POINTER, cb_TestSRP);
	
	Ctrl=NewCtrl(Pnl, CTRL_ROUND_LED_LS, "Round LED", 70, 30);
	SetCtrlAttribute (Pnl, Ctrl, ATTR_CALLBACK_FUNCTION_POINTER, cb_TestSRP);
	SetCtrlAttribute (Pnl, Ctrl, ATTR_CTRL_MODE, VAL_HOT);
	
	DisplayPanel(Pnl);
	RunUserInterface();
	return 0;
}

#endif

